<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

// GET - Todos los estudiantes 
$app->get('/api/estudiantes', function(Request $request, Response $response){
  $sql = "SELECT * FROM estudiantes";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->query($sql);

    if ($resultado->rowCount() > 0){
      $estudiantes = $resultado->fetchAll(PDO::FETCH_OBJ);
      echo json_encode($estudiantes);
    }else {
      echo json_encode("No existen estudiantes en la BBDD.");
    }
    $resultado = null;
    $db = null;
  }catch(PDOException $e){
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
}); 

// GET - Recuperar estudiantes por ID 
$app->get('/api/estudiantes/{id}', function(Request $request, Response $response){
  $id_estudiante = $request->getAttribute('id');
  $sql = "SELECT * FROM estudiantes WHERE id = $id_estudiante";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->query($sql);

    if ($resultado->rowCount() > 0){
      $estudiante = $resultado->fetchAll(PDO::FETCH_OBJ);
      echo json_encode($estudiante);
    }else {
      echo json_encode("No existe estudiante en la BBDD con este ID.");
    }
    $resultado = null;
    $db = null;
  }catch(PDOException $e){
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
}); 


// POST - Crear nuevo estudiantes 
$app->post('/api/estudiantes', function(Request $request, Response $response){
   $nombre = $request->getParam('nombre');
   $apellidos = $request->getParam('apellido');
   $email = $request->getParam('email');
   $direccion = $request->getParam('direccion');
  
  $sql = "INSERT INTO estudiantes (nombre, apellido, email, direccion) VALUES 
          (:nombre, :apellido, :email, :direccion)";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->prepare($sql);

    $resultado->bindParam(':nombre', $nombre);
    $resultado->bindParam(':apellido', $apellidos);
    $resultado->bindParam(':email', $email);
    $resultado->bindParam(':direccion', $direccion);

    $resultado->execute();
    echo json_encode("Nuevo estudiante guardado.");  

    $resultado = null;
    $db = null;
  }catch(PDOException $e){
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
}); 



// PUT - Modificar estudiantes 
$app->put('/api/estudiantes/{id}', function(Request $request, Response $response){
   $id_estudiante = $request->getAttribute('id');
   $nombre = $request->getParam('nombre');
   $apellido = $request->getParam('apellido');
   $email = $request->getParam('email');
   $direccion = $request->getParam('direccion');
  
  $sql = "UPDATE estudiantes SET
          nombre = :nombre,
          apellido = :apellido,
          email = :email,
          direccion = :direccion
        WHERE id = $id_estudiante";
     
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->prepare($sql);

    $resultado->bindParam(':nombre', $nombre);
    $resultado->bindParam(':apellido', $apellido);
    $resultado->bindParam(':email', $email);
    $resultado->bindParam(':direccion', $direccion);

    $resultado->execute();
    echo json_encode("Estudiante modificado.");  

    $resultado = null;
    $db = null;
  }catch(PDOException $e){
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
}); 


// DELETE - borrar estudiante 
$app->delete('/api/estudiantes/{id}', function(Request $request, Response $response){
   $id_estudiante = $request->getAttribute('id');
   $sql = "DELETE FROM estudiantes WHERE id = $id_estudiante";
     
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->prepare($sql);
     $resultado->execute();

    if ($resultado->rowCount() > 0) {
      echo json_encode("Estudiante eliminado.");  
    }else {
      echo json_encode("No existe estudiante con este ID.");
    }

    $resultado = null;
    $db = null;
  }catch(PDOException $e){
    echo '{"error" : {"text":'.$e->getMessage().'}';
  }
}); 







